package com.employeeservice;

import java.util.HashSet;
import java.util.Scanner;

import com.employee.Employees;

public class EmployeeService {

	HashSet<Employees> employeeset = new HashSet<Employees>();

	Employees employees1 = new Employees(101, "Shital", 23, "IT", "Developer", 25000);
	Employees employees2 = new Employees(102, "Mohan", 25, "CO", "Tester", 28000);
	Employees employees3 = new Employees(103, "Bina", 24, "Admin", "Devops Eng", 5000);
	Employees employees4 = new Employees(104, "Max", 26, "CO", "System Eng", 40000);

	Scanner sc = new Scanner(System.in);
	boolean found = false;

	int id;
	String name;
	int age;
	String department;
	String desiganation;
	double salary;

	public EmployeeService() {

		employeeset.add(employees1);
		employeeset.add(employees2);
		employeeset.add(employees3);
		employeeset.add(employees4);

	}

	// View all Employees
	public void viewAllEmployees() {
		for (Employees employee : employeeset) {
			System.out.println(employee);
		}
	}

	// View Employees based on there ID
	public void viewEmployees() {

		System.out.println("Enter ID:");
		id = sc.nextInt();
		for (Employees employee : employeeset) {
			if (employee.getId() == id) {
				System.out.println(employee);
				found = true;

			}

		}
		if (!found) {
			System.out.println("Employee with this id is not present");
		}
	}

	// Update the employees
	public void updateEmployees() {
		System.out.println("Enter the Id:");
		id = sc.nextInt();
		boolean found = false;
		for (Employees employee : employeeset) {
			if (employee.getId() == id) {
				System.out.println("Enter the name:");
				name = sc.next();
				System.out.println("Enter the new Salary:");
				salary = sc.nextDouble();
				System.out.println("Enter the new desiganation:");
				desiganation = sc.next();
				employee.setName(name);
				employee.setSalary(salary);
				employee.setDesiganation(desiganation);
				System.out.println("Updated Detail of employee are:");
				System.out.println(employee);
				found = true;

			}
		}
		if (!found) {
			System.out.println("Employee is not present");
		} else {
			System.out.println("Employee detail updated successfully!!");
		}
	}

	// Delete the Employee
	public void deleteEmployees() {
		System.out.println("Enter Id:");
		id = sc.nextInt();
		boolean found = false;
		Employees employeedelete = null;
		for (Employees employee : employeeset) {
			if (employee.getId() == id) {
				employeedelete = employee;
				found = true;

			}
		}
		if (!found) {
			System.out.println("Employee is not avilable");
		} else {
			employeeset.remove(employeedelete);
			System.out.println("Emmployee deleted successfully!!");
		}
	}

	// Add the employee
	public void addEmployees() {
		System.out.println("Enter Id:");
		id = sc.nextInt();
		System.out.println("Enter name:");
		name = sc.next();
		System.out.println("Enter age:");
		age = sc.nextInt();
		System.out.println("Enter Desiganation:");
		desiganation = sc.next();
		System.out.println("Enter Department:");
		department = sc.next();
		System.out.println("Enter Salary:");
		salary = sc.nextDouble();
		Employees emp = new Employees(id, name, age, desiganation, department, salary);
		employeeset.add(emp);
		System.out.println(emp);
		System.out.println("Employee addeed successfully!!");

	}
}
