package com.dao;

import java.util.List;

import javax.transaction.SystemException;
import javax.transaction.Transaction;

import org.hibernate.Session;

import com.model.Student;

import com.util.HibernateUtil;

public class StudentDao {

	// save Student
	// get All Student
	// get Student By Id
	// delete Student

	public void saveStudent(Student student) throws IllegalStateException, SystemException {

		Transaction transaction = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = (Transaction) session.beginTransaction();
			// save the student object
			session.save(student);
			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				
					transaction.rollback();
				
			}

		}
	}

	public void updateStudent(Student student) throws IllegalStateException, SystemException {

		Transaction transaction = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = (Transaction) session.beginTransaction();
			// save the student object
			session.saveOrUpdate(student);
			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				
					transaction.rollback();
				
			}

		}
	}

	public Student getStudentById(long id) throws IllegalStateException, SystemException {

		Transaction transaction = null;
		Student student = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = (Transaction) session.beginTransaction();
			// get the student object
			student = session.get(Student.class, id);
			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				
					transaction.rollback();
				
			}

		}
		return student;
	}

	@SuppressWarnings("unchecked")
	public List<Student> getAllStudent() throws IllegalStateException, SystemException {

		Transaction transaction = null;
		List<Student> student = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = (Transaction) session.beginTransaction();
			// get the student object
			student = session.createQuery("from Student").list();
			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				
					transaction.rollback();
				
			}

		}
		return student;
	}

	public void deleteStudent(long id) throws IllegalStateException, SystemException {

		Transaction transaction = null;
		Student student = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = (Transaction) session.beginTransaction();
			student = session.get(Student.class, id);
			// get the student object
			session.delete(student);
			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				
					transaction.rollback();
				
			}

		}
	}
}
