package com.main;



import java.util.List;

import javax.transaction.SystemException;

import com.dao.StudentDao;
import com.model.Student;

public class Main {
	public static void main(String[] args) throws IllegalStateException, SystemException {
		
		StudentDao studentdao = new StudentDao();
		//test saveStudent
		Student student = new Student("Sujeet","Kumar","sujeet@gmail.com");
		
		studentdao.saveStudent(student);
		//test updateStudent
		student.setFirstName("Ram");
		studentdao.updateStudent(student);
		//test getStudentById
		@SuppressWarnings("unused")
		Student student2 = studentdao.getStudentById(student.getId());
		//test getAllStudent
		List<Student> students = studentdao.getAllStudent();
		students.forEach(student1-> System.out.println(student1.getId()));
		//test deleteStudent
		studentdao.deleteStudent(student.getId());
	}

}
